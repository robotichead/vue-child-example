Build using Laravel - [Laravel](https://laravel.com)
<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Run Instance

1. Use `git clone git@gitlab.com:robotichead/vue-child-example.git` to download the source
2. Use a terminal and cd (change directory) into the source folder
3. Use npm to install all required packages: `npm run install`
4. Use composer to install required packages for Laravel: `composer install`
5. Build the JavaScript files: `npm run prod`
6. Use php's artisan to serve a server: `php artisan serve`

You can now visit your `localhost:8000/` to view the example

## License

This Laravel/Vue.js example is under the  [MIT license](https://opensource.org/licenses/MIT).
