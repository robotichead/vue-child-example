<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// User is getting the landing page
Route::get('/', function () {
    return view('welcome');
});

// User posted values - just send them to the welcome page
Route::post('/', function () {
    return view('welcome');
});
